<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/meme/import', 'MemeController@import')->name('memes.import');
Route::get('/meme/all', 'MemeController@all')->name('memes.all');
Route::get('/meme/page/{page?}', 'MemeController@all')->name('memes.page');
Route::get('/meme/id/{id}', 'MemeController@show')->name('memes.show');
Route::get('/meme/popular', 'MemeController@showPopular')->name('memes.popular');
Route::post('/meme/create', 'MemeController@store')->name('memes.create');