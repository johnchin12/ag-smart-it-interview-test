**API Url** - www.johndevwebsite.me/api

Retrive data from **https://interview.agsmartit.com/index.php** : www.johndevwebsite.me/api/meme/import

**GET Method**
1. Get all images data          : www.johndevwebsite.me/api/meme/all
2. Get image data based on id   : www.johndevwebsite.me/api/meme/id/2
3. Get image data based on page : www.johndevwebsite.me/api/meme/page/3
4. Get the most popular image   : www.johndevwebsite.me/api/meme/popular

**POST Method**
1. Insert new image data        : www.johndevwebsite.me/api/meme/create