<?php
namespace App\Imports;
use KubAT\PhpSimple\HtmlDomParser;

class DataImport{
    public function process(){
        ini_set('max_execution_time', '180');

        $main_url = "https://interview.agsmartit.com/index.php";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $main_url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        $dom = HtmlDomParser::str_get_html($response);

        if(!empty($dom)){
            $paginations = [];
            foreach($dom->find('nav > ul.pagination > li') as $pagination_link){
                if(!$pagination_link->find('a', 0)->hasAttribute('aria-label')){
                    $paginations[] = $pagination_link->find('a', 0)->href;
                }
            }

            $memes = [];

            foreach($paginations as $pagination){
                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $main_url . $pagination);
                curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
                $response2 = curl_exec($ch2);
                curl_close($ch2);

                $dom2 = HtmlDomParser::str_get_html($response2);

                $page_num = str_replace('?page=', '', $pagination);

                foreach($dom2->find('div.well > div.row > div.col-sm-4') as $meme_container){
                    $img = $meme_container->find('div.thumbnail.meme-frame > img.meme-img', 0)->src;
                    $name = $meme_container->find('div.meme-name > h6', 0)->plaintext;

                    $memes[] = array(
                        'name' => $name,
                        'url' => $img,
                        'page' => $page_num
                    );
                }
            }

            return $memes;
        }
    }
}
