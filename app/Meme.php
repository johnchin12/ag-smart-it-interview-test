<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Meme extends Model
{
    //
    protected $guarded = [];

    public function getUrlAttribute($value){

        if(Str::contains($value, 'https://')){
            return $value;
        }else{
            return  Storage::url($value);
        }
    }
}
