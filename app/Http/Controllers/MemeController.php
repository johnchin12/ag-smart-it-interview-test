<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\DataImport;

use App\Meme;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class MemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $limit = 9;

    public function index()
    {
        //
        
    }

    /**
     * Code that contain with ---------- is use for situation when page data is not stored in database
    */

    public function all($filter_page = ''){
        if($filter_page){
            //----------
            //$memes = Meme::orderBy('id', 'asc')->offset(((int)$filter_page - 1)*$this->limit)->take($this->limit)->get();
            //----------

            $memes = Meme::where('page', $filter_page)->orderBy('id', 'asc')->get();
        }else{
            $memes = Meme::orderBy('id', 'asc')->get();
        }

        //----------
        // $counter = 0;
        // $page_num = $filter_page ? (int)$filter_page : 1;
        //----------
        
        $memes_data = [];
        foreach($memes as $meme){
            $memes_data[] = array(
                'id' => $meme->id,
                'name' => $meme->name,
                'url' => $meme->url,
                'page' => $meme->page, //----------$page_num
                'requestCount' => $meme->request_count
            );

            //----------
            /*$counter++;

            if($counter == $this->limit){
                $counter = 0;
                $page_num++;
            }*/
            //----------
        }

        return response(json_encode($memes_data, JSON_PRETTY_PRINT),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty($request->data)){

            $meme_to_insert = [];

            $max_page = Meme::max('page');
            $count = Meme::where('page', $max_page)->count();

            $page = $max_page;

            foreach($request->data as $meme){
                if($count >= $this->limit){
                    $page++;

                    $count = 0;
                }

                $meme_to_insert[] = array(
                    'name' => $meme['name'],
                    'url' => $meme['url'],
                    'page' => $page
                );

                $count++;
            }

            Meme::insert($meme_to_insert);

            return response()->json([
                "message" => "Memes are created"
              ], 404);
        }else{
            return response()->json([
                "message" => "Nothing to insert"
              ], 404);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Meme::where('id', $id)->exists()){
            //----------
            /*$page = 1;

            while(($this->limit*$page-1) < ($id <= $this->limit*$page)){
                $page++;
            }*/
            //----------
            
            $meme = Meme::find($id);

            $new_request_count = (int)$meme->request_count + 1;

            $meme->update(['request_count'=>$new_request_count]);

            return response(json_encode(array(
                'id' => $meme->id,
                'name' => $meme->name,
                'url' => $meme->url,
                'page' => $meme->page, //----------$page
                'requestCount' => $new_request_count
            ), JSON_PRETTY_PRINT), 200);
        }else{
            return response()->json([
                "message" => "Meme not found"
              ], 404);
        }
        
    }

    public function showPopular(){
        if(Meme::orderBy('request_count', 'desc')->exists()){
            $meme = Meme::orderBy('request_count', 'desc')->firstOrFail();

            //----------
            /*$page = 1;

            while(($this->limit*$page-1) < ($meme->id <= $this->limit*$page)){
                $page++;
            }*/
            //----------

            return response(json_encode(array(
                'id' => $meme->id,
                'name' => $meme->name,
                'url' => $meme->url,
                'page' => $meme->page, //----------$page
                'requestCount' => $meme->request_count
            ), JSON_PRETTY_PRINT),200);
        }else{
            return response()->json([
                "message" => "No meme found"
              ], 404);
        }
        
    }
    
    public function import(){
        $import = new DataImport();

        $memes = $import->process();

        Meme::truncate();

        Meme::insert($memes);

        return response()->json([
            "message" => "meme import complete"
        ], 201);
    }
}
